from sys import argv
import random
from TwitterSearch import *

ts = TwitterSearch(consumer_key=argv[2], consumer_secret=argv[3], access_token=argv[4], access_token_secret=argv[5])


def reduce(lst, new_size):
    while len(lst) > new_size:
        random.shuffle(lst)
        lst = lst[0: len(lst) - 1]
    return lst


def translate(original, list_of_words):
    if len(list_of_words) == 0:
        return original
    tso = TwitterSearchOrder()
    tso.set_keywords(reduce(list_of_words, 10))
    statuses = ts.search_tweets(tso)['content']['statuses']
    return random.choice(statuses)['text'].replace("\t", " ").replace("\n", " ").replace("\r", " ") \
        if len(statuses) > 0 else translate(original, reduce(list_of_words, len(list_of_words) - 1))


with open(argv[1]) as text:
    for line in text:
        print(translate(line, line.lower().split()))
