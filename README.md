# Just another Tweetifier

This Python3 script translates any random text into tweets. The output does not necessarily make sense, but can be fun.

## Requirements

- A recent version of Python 3
- pip and virtualenv

## Setup

Create a virtual environment for this project:

    virtualenv env -p python3
    source env/bin/activate

Install the packages:

    pip install -r dependencies.txt

## Run it

You will probably need to apply for some twitter access tokens at: https://apps.twitter.com/
Create a new app and use the keys and secrets to run the script:

    python3 twitter_translator.py random_text.txt consumer_key consumer_secret access_token access_token_secret > random_text.tweetified

Enjoy.

**Note:** Due to the limited number of lines, the script does not remove punctuation from the file, so the input needs to be cleaned up before processing.